const fetch = require('node-fetch');
const fs = require('fs');

async function reactToFindings() {
    let findings = fs.readFileSync(process.env.FINDINGS_PATH);

    if (!findings) {
        console.log("I have no findings for you today");
        return;
    }

    for (let i = 0; i < findings.length; i++) {
        await reactToFinding(findings[i]);
    }
}

async function reactToFinding(finding) {
    fetch("https://api.stackexchange.com/2.2/posts/" + finding.answer_id + "/comments/add", {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*",
            "key": process.env.KEY,
            "access_token": process.env.ACSESS_TOKEN
        },
        body: finding.finding
    }).then(response => {
        if (response.status >= 200 && response.status < 300) {
            response.json().then(json => {
               console.log(json);

            });

        } else {
            console.log(response)
        }
    }).catch(
        error => {
            console.log(error);
        }
    );

}

module.exports.reactToFindings = reactToFindings;