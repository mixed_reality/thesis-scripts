const receiver = require('./data-receiver');
const preparator = require('./data-preperator');
const analyser = require('./code-analyser');
const reactor = require('./findings-reactor');
require('dotenv').config();

let hoursdelta = (24 * 0);

//Create date of today at 00:00:00
let today = new Date();
today.setHours(0, 0, 0, 0);
today.setHours(today.getHours() - hoursdelta);
let unix_today = Math.round(today.getTime() / 1000);

//Create date of yesterday at 00:00:01
let yesterday = new Date();
yesterday.setHours(0, 0, 1, 0);
yesterday.setHours(yesterday.getHours() - 24 - hoursdelta);
let unix_yesterday = Math.round(yesterday.getTime() / 1000);

let two_days_ago = new Date(today.getFullYear(), today.getMonth(), today.getDate());
two_days_ago.setHours(0, 0, 0, 0);
two_days_ago.setHours(two_days_ago.getHours() - 48);
let unix_two_days_ago = Math.round(two_days_ago.getTime() / 1000);

/*
 * Max Page size is 100
 */
let tags = ['php', 'c', 'c++'];

let question_filter = {
    client_id: process.env.CLIENT_ID,
    key: process.env.KEY,
    pagesize: 100,
    sort: 'activity',
    order: 'asc',
    body: true,
    fromdate: unix_two_days_ago,
    todate: unix_today
};

let answer_filter = {
    client_id: process.env.CLIENT_ID,
    key: process.env.KEY,
    pagesize: 50,
    sort: 'activity',
    order: 'asc',
    filter: 'withbody',
    fromdate: unix_yesterday,
    todate: unix_today
};


/*
 * Run spider, run
 */
spider();

async function spider() {
    /*
     * Wait for all answers
     */
    console.log("[INFO] Starting script for answers from " + yesterday.toLocaleDateString() + " to " + today.toLocaleDateString());
   // let answers = await receiver.getAllAnswersWithCodeWithFile();
    let answers = await receiver.getAllAnswersWithCode(question_filter, answer_filter, tags);
    /*
     * Create files from the answers
     */
    await preparator.createTestFiles(answers);
    /*
     * run static code analysis
     */
    await analyser.analyseCodes();
    /*
     * React to findings
     */
    //IN CONSTRUCTION await reactor.reactToFindings();

}


