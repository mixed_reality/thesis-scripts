const fs = require('fs');
const util = require('util');
const awExec = util.promisify(require('child_process').exec);
const exec = require('child_process').exec;

const FILES_DIRECTORY = "./test-files/";
const FILES_FULL_PATH = "/home/spider/thesis-scripts/test-files/";
const VULN_PREFIX = " = = = =";
/*
 * here all findings are stored
 */
let findings = [];

/*
 * runs all files from ./test-files folder with
 * the static analysis tools
 */
async function analyseCodes() {
    let files = fs.readdirSync(FILES_DIRECTORY);

    if (files === undefined) {
        console.log("[ERROR] undefined files");
    } else {
        for (const file of files) {
            if (file.endsWith(".cpp") || file.endsWith(".c")) {
                //use Flawfinder
                await analyseWithFlawfinder(file);
            }
            else if (file.endsWith(".php")) {
                //use Progpilot
                await analyseWithProgpilot(file);
            }
            else if (file.endsWith(".js")) {
                //TODO find an analyser
            }
        }

        console.log("Before: " + findings.length);
        findings = combineFindings(findings);
        console.log("After: " + findings.length);

        //Delete old findings
        if (fs.existsSync(process.env.FINDINGS_PATH))
            fs.unlinkSync(process.env.FINDINGS_PATH);
        //Write findings to json
        fs.writeFileSync(process.env.FINDINGS_PATH, JSON.stringify(findings), 'utf8');
    }
}

function combineFindings(findings) {
    let combinedFindings = [];

    for (let i = 0; i < findings.length; i++) {
        //check if already exists in combined findings

        let alreadyIn = false;
        for (let j = 0; j < combinedFindings.length; j++) {
            if (findings[i].answer_id === combinedFindings[j].answer_id) {
                combinedFindings[j].finding += findings[i].finding;
                alreadyIn = true;
                break;
            }
        }

        if (!alreadyIn) {
            combinedFindings.push(findings[i]);
        }

    }
    return combinedFindings;
}


async function analyseWithRats(file) {
    let {err, stdout, stderr} = await awExec("./rats --resultsonly " + FILES_FULL_PATH + file,
        {cwd: "/home/spider/rats-2.4/"});
    if (err) {
        console.error(err);
        return;
    }
    //If there are findings
    if (stdout.length > 0) {
        let ids = file.split("_");
        let question_id = parseInt(ids[0]);
        let answer_id = parseInt(ids[1]);
        findings.push({
            question_id: question_id,
            answer_id: answer_id,
            finding: stdout
        })

    }
}

async function analyseWithFlawfinder(file) {
    let {err, stdout, stderr} = await awExec("flawfinder " + FILES_FULL_PATH + file);
    if (err) {
        console.error(err);
        return;
    }
    //If there are findings
    if (stdout.length > 0 && stdout.includes("FINAL RESULTS:") && stdout.includes(FILES_FULL_PATH + file)) {
        let findingTextPrefix = "FINAL RESULTS:";
        let findingText = stdout.substring(
            stdout.indexOf(findingTextPrefix) + findingTextPrefix.length,
            stdout.indexOf("ANALYSIS SUMMARY:"));

        let findingsArray = findingText.split(FILES_FULL_PATH + file);
        if (findingsArray.toString().replace(/\r?\n|\r/g, "").length > 0) {
            let ids = file.split("_");
            let question_id = parseInt(ids[0]);
            let answer_id = parseInt(ids[1]);
            findings.push({
                question_id: question_id,
                answer_id: answer_id,
                finding: findingsArray.toString()
            });
        }
    }
}

async function analyseWithProgpilot(file) {
    const stdout = await getProgpilotOutput(file);

    let findingsObject = JSON.parse(stdout);
    try {

        if (findingsObject.length > 0) {
            let ids = file.split("_");
            let question_id = parseInt(ids[0]);
            let answer_id = parseInt(ids[1]);
            let findingText = "";
            for (let i = 0; i < findingsObject.length; i++) {
                if (findingsObject[i].hasOwnProperty('source_line')) {
                    findingText += "A " + findingsObject[i].vuln_name +
                        " (" + findingsObject[i].vuln_cwe + ")" +
                        " vulnerability found at line " + findingsObject[i].source_line[0] +
                        " in '" + findingsObject[i].source_name[0] + "'. \n";
                } else if (findingsObject[i].hasOwnProperty('vuln_line')) {
                    findingText += "A " + findingsObject[i].vuln_name +
                        " (" + findingsObject[i].vuln_cwe + ")" +
                        " vulnerability found at line " + findingsObject[i].vuln_line +
                        ". \n";
                }
            }

            findings.push({
                question_id: question_id,
                answer_id: answer_id,
                finding: findingText
            })
        }
    } catch (err) {

        console.log(findingsObject)
    }
}

function getProgpilotOutput(file) {
    return new Promise(resolve => {
            exec("php progpilot.phar " + FILES_FULL_PATH + file,
                {cwd: "/home/spider/progpilot/"},
                (error, stdout, stderr) => {
                    resolve(stdout);
                });
        }
    );
}

async function analyseWithWap(file) {
    let {err, stdout, stderr} = await awExec("yes | java -jar wap.jar -a -all " + FILES_FULL_PATH + file,
        {cwd: "/home/spider/wap-2.1"});
    if (err) {
        console.error(err);
        return;
    }

    let summery_SQL = stdout.substring(stdout.indexOf("Type of Analysis: SQLI"));
    summery_SQL = summery_SQL.substring(0, summery_SQL.indexOf("+"));

    let summery_XXS = stdout.substring(stdout.indexOf("Type of Analysis: XSS"));
    summery_XXS = summery_SQL.substring(0, summery_XXS.indexOf("+"));

    let summery_CI = stdout.substring(stdout.indexOf("Type of Analysis: RFI/LFI/DT/SCD/OS/Eval"));

    let finding = "";

    if (summery_XXS.includes("Information")) {
        //Found XXS
        finding += summery_XXS.substring(summery_XXS.lastIndexOf(VULN_PREFIX) + VULN_PREFIX.length);
    }

    if (summery_SQL.includes("Information")) {
        //Found SQL
        finding += summery_SQL.substring(summery_XXS.lastIndexOf(VULN_PREFIX) + VULN_PREFIX.length);
    }

    if (summery_CI.includes("Information")) {
        //Found CI
        finding += summery_CI.substring(summery_CI.lastIndexOf(VULN_PREFIX) + VULN_PREFIX.length);
    }

    if (finding.length > 0) {
        let ids = file.split("_");
        let question_id = parseInt(ids[0]);
        let answer_id = parseInt(ids[1]);
        findings.push({
            question_id: question_id,
            answer_id: answer_id,
            finding: finding
        })
    }
}

module.exports.analyseCodes = analyseCodes;