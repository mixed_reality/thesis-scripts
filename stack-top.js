/*
 * Checks the top 100 questions of the checked languages of all time
 */
const preparator = require("./data-preperator");
const receiver = require("./data-receiver");
const analyser = require("./code-analyser");
require('dotenv').config();


/*
 * Max Page size is 100
 */
let tags = ['php', 'c', 'c++', 'javascript', 'node.js'];

let question_filter = {
    client_id: process.env.CLIENT_ID,
    key: process.env.KEY,
    pagesize: 100,
    min: 10,
    sort: 'votes',
    order: 'desc',
    body: true
};

let answer_filter = {
    client_id: process.env.CLIENT_ID,
    key: process.env.KEY,
    pagesize: 50,
    sort: 'votes',
    order: 'asc',
    filter: 'withbody',
};

top_spider();

async function top_spider() {
    /*
     * Wait for all answers
     */
    let answers = await receiver.getAllAnswersWithCode(question_filter, answer_filter, tags, true, true);
    /*
     * Create files from the answers
     */
    await preparator.createTestFiles(answers);
    /*
     * run static code analysis
     */
    await analyser.analyseCodes();
    /*
     * React to findings
     */
    //IN CONSTRUCTION await reactor.reactToFindings();

}
