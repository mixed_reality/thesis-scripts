const fs = require('fs');
const path = require("path");
const decode = require('unescape');

//Only use codes with more than 5 chars
const MIN_CODE_LENGTH = 6;
const FILES_DIRECTORY = "./test-files/";

async function createTestFiles(answersWithTags) {
    //cleans directory before creating new files
    await cleanDirectory();
    for (let i = 0; i < answersWithTags.length; i++) {
        if (answersWithTags[i].tags.includes('php')) {
            createPHPFiles(answersWithTags[i]);

        } else if (answersWithTags[i].tags.includes('c++')) {
            createCPPFiles(answersWithTags[i]);

        } else if (answersWithTags[i].tags.includes('c')) {
            createCFiles(answersWithTags[i]);

        } else if (answersWithTags[i].tags.includes('javascript')
            || answersWithTags[i].tags.includes('node.js')) {
            createJSFiles(answersWithTags[i]);

        }
    }
}

async function cleanDirectory() {
    let files = fs.readdirSync(FILES_DIRECTORY);
    if (files === undefined) {
        console.log("[ERROR] undefined files");
    } else {
        for (const file of files) {
            fs.unlink(path.join(FILES_DIRECTORY, file), err => {
                if (err) throw err;
            });
        }
    }
}

function createPHPFiles(answerWithPhpCodes) {
    for (let i = 0; i < answerWithPhpCodes.codes.length; i++) {
        let code = decode(answerWithPhpCodes.codes[i]);
        if (code.length >= MIN_CODE_LENGTH) {
            let filename = FILES_DIRECTORY + answerWithPhpCodes.question_id + "_" + answerWithPhpCodes.answer_id + "_" + i + ".php";
            fs.writeFileSync(filename, code);
        }
    }
}

function createCPPFiles(answerWithCppCodes) {
    for (let i = 0; i < answerWithCppCodes.codes.length; i++) {
        let code = decode(answerWithCppCodes.codes[i]);
        if (code.length >= MIN_CODE_LENGTH) {
            let filename = FILES_DIRECTORY + answerWithCppCodes.question_id + "_" + answerWithCppCodes.answer_id + "_" + i + ".cpp";
            fs.writeFileSync(filename, code);
        }
    }
}

function createCFiles(answerWithCCodes) {
    for (let i = 0; i < answerWithCCodes.codes.length; i++) {
        let code = decode(answerWithCCodes.codes[i]);
        if (code.length >= MIN_CODE_LENGTH) {
            let filename = FILES_DIRECTORY + answerWithCCodes.question_id + "_" + answerWithCCodes.answer_id + "_" + i + ".c";
            fs.writeFileSync(filename, code);
        }
    }
}

function createJSFiles(answerWithCCodes) {
    for (let i = 0; i < answerWithCCodes.codes.length; i++) {
        let code = decode(answerWithCCodes.codes[i]);
        if (code.length >= MIN_CODE_LENGTH) {
            let filename = FILES_DIRECTORY + answerWithCCodes.question_id + "_" + answerWithCCodes.answer_id + "_" + i + ".js";
            fs.writeFileSync(filename, code);
        }
    }
}

module.exports.createTestFiles = createTestFiles;
module.exports.FILES_DIRECOTRY = FILES_DIRECTORY;