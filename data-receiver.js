const stackexchange = require("stackexchange");
const fs = require('fs');

let options = {version: 2.2};
let context = new stackexchange(options);


async function getAllAnswersWithCode(question_filter, answer_filter, tags, only_accepted = false, only_first = false) {
    console.log("[INFO] run spider, run...");
    console.log("[INFO] Getting all questions from " + question_filter.fromdate + " to " + question_filter.todate);
    let allAnsweres = [];
    for (let i = 0; i < tags.length; i++) {
        let questionIdsWithTags = await getAllQuestionIdsWithTags(tags[i], question_filter, only_first);

        //Wait for an second to prevent abuse
        await sleep(1000);

        console.log("[INFO] Found " + questionIdsWithTags.length + " Questions with tag " + tags[i]);
        let answers = await getAllAnswers(questionIdsWithTags, answer_filter, only_accepted);
        //Wait for an second to prevent abuse
        await sleep(1000);

        let answersToAdd = extractCodeFromAnswers(answers, questionIdsWithTags);
        console.log("[INFO] Found " + answersToAdd.length + " Answers with with tag " + tags[i]);
        allAnsweres = allAnsweres.concat(answersToAdd);
    }
    //Filter out duplicate answers
    allAnsweres = filterArray(allAnsweres);

    //Delete old answers
    if (fs.existsSync('answers.json'))
        fs.unlinkSync('answers.json');
    //Write answers to work with a file for development
    fs.writeFileSync('answers.json', JSON.stringify(allAnsweres), 'utf8');

    return allAnsweres;
}

function filterArray(arr) {
    return arr.filter(function(item, index){
        return arr.indexOf(item) >= index;
    });
}


function getAllAnswersWithCodeWithFile() {
    let rawdata = fs.readFileSync('answers.json');
    return JSON.parse(rawdata);
}


async function getAllQuestionIdsWithTags(tag, question_filter, only_first) {
    let page = 1;
    let has_more = true;
    let questionIds = [];
    do {
        let results = await getQuestionPage(page, tag, question_filter);
        //get question ids from questions with answers
        for (let i = 0; i < results.items.length; i++) {
            let item = results.items[i];
            if (item.answer_count > 0) {
                questionIds.push({
                    question_id: item.question_id,
                    tags: item.tags
                });
            }
        }
        page++;
        //Wait for an second every 30 Requests to prevent abuse
        if (page % 30 === 0) {
            await sleep(1000);
        }
        //if only first page requested
        has_more = only_first ? false : results.has_more;
    } while (has_more);

    return questionIds;
}


function getQuestionPage(page, tag, question_filter) {
    question_filter.page = page;
    question_filter.tagged = tag;
    return new Promise(resolve => {
            context.questions.questions(question_filter, function (err, results) {
                if (err) throw err;
                resolve(results)
            })
        }
    );
}


async function getAllAnswers(questionIdsWithTags, answer_filter, only_accepted) {
    let answers = [];
    if (questionIdsWithTags.length > 0) {
        if (questionIdsWithTags.length <= 100) {
            answers = await getAnswersOfIds(questionIdsWithTags, answer_filter);
        } else {
            for (let i = 0; i < questionIdsWithTags.length; i += 100) {
                if (questionIdsWithTags.length - 1 > i + 100) {
                    answers = answers.concat(
                        await getAnswersOfIds(questionIdsWithTags.slice(i, i + 100), answer_filter)
                    );
                } else {
                    answers = answers.concat(
                        await getAnswersOfIds(questionIdsWithTags.slice(i, questionIdsWithTags.length - 1), answer_filter)
                    );
                }
            }
        }
    }
    if (!only_accepted) {
        console.log("[INFO] Returning all answers");
        return answers;
    } else {
        //filter out accepted answers
        let filteres_answers = [];
        for (let i = 0; i < answers.length; i++) {
            if (answers[i].is_accepted) {
                filteres_answers.push(answers[i]);
            }
        }
        console.log("[INFO] Returning all accepted answers");
        return filteres_answers;
    }
}

//Max ids are 100
async function getAnswersOfIds(questionIdsWithTags, answer_filter) {
    let page = 1;
    let has_more = true;
    let answers = [];
    let questionIds = extractIdsFromQuestions(questionIdsWithTags);
    do {
        let results = await getAnswerPage(page, questionIds, answer_filter);
        //console.log(results);
        if (results.hasOwnProperty("items") && results.items.length > 0)
            answers = answers.concat(results.items);

        page++;
        //Wait for an second every 30 Requests to prevent abuse
        if (page % 30 === 0) {
            await sleep(1000);
        }
        has_more = results.has_more;
    } while (has_more);

    return answers;
}

function extractIdsFromQuestions(questionsWithIds) {
    let ids = [];
    for (let i = 0; i < questionsWithIds.length; i++) {
        ids.push(questionsWithIds[i].question_id);
    }

    return ids;
}

function getAnswerPage(page, questionIds, answer_filter) {
    answer_filter.page = page;
    let ids = questionIds.slice(0, 100);
    if(ids.length === 0)
        return [];
    return new Promise(resolve => {
            context.questions.answers(answer_filter, function (err, results) {
                if (err) throw err;
                resolve(results)
            }, ids)
        }
    ).catch(err => console.log(ids));
}

function getTagsFromQuestion(questionIdsWIthTags, question_id) {
    for (let i = 0; i < questionIdsWIthTags.length; i++) {
        if (questionIdsWIthTags[i].question_id === question_id) {
            return questionIdsWIthTags[i].tags;
        }
    }
    return [];
}

function extractCodeFromAnswers(answers, questionIdsWIthTags) {
    let answersWithCode = [];
    for (let i = 0; i < answers.length; i++) {
        if (answers[i].hasOwnProperty("body")) {
            let codes = getCodesTagFromHtml(answers[i].body);
            let tags = getTagsFromQuestion(questionIdsWIthTags, answers[i].question_id);
            answersWithCode.push({
                answer_id: answers[i].answer_id,
                question_id: answers[i].question_id,
                codes: codes,
                tags: tags
            })
        }
    }

    return answersWithCode;
}

function getCodesTagFromHtml(html) {
    let codes = [];

    if (!html.includes("<code>") && !html.includes("</code>"))
        return codes;

    do {
        //cut everything to the code block
        html = html.substring(html.indexOf("<code>") + 6);
        //extract code block
        let code = html.substring(0, html.indexOf("</code>"));
        //cut code block out of html
        html = html.substring(html.indexOf("</code>") + 7);
        //add code to codes
        codes.push(code);
    } while (html.includes("<code>") && html.includes("</code>"));

    return codes;
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}


module.exports.getAllAnswersWithCode = getAllAnswersWithCode;
module.exports.getAllAnswersWithCodeWithFile = getAllAnswersWithCodeWithFile;