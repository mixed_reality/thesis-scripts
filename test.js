const express = require('express');
const router = express.Router();
let options = {
    clientId: '14939',
    clientSecret: '7U)eQRms)WX1U024Bd*rtg((',
    callbackURL: 'http://localhost:3000/auth/stack-exchange/callback',
    scope: ['write_access', 'no_expiry']
};

console.log(options);


const simpleOAuth2StackExchange = require('simple-oauth2-stack-exchange');

const stackExchange = simpleOAuth2StackExchange.create(options);

/* GET home page. */
router.get('/', (req, res, next) => {
    res.render('index', {title: 'Express'});
});


// Ask the user to authorize.
router.get('/auth/stack-exchange', stackExchange.authorize);

// Exchange the token for the access token.
router.get('/auth/stack-exchange/callback', stackExchange.accessToken, (req, res) => {
    return res.status(200).json(req.token);
});


module.exports = router;

