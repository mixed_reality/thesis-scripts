const express = require("express");
const test = require("./test");

const app = express();

app.use("/", test);

module.exports = app;
